package com.mmall.util;

import org.apache.commons.lang3.StringUtils;

/**
 * 部门LevelUtil与规则
 */
public class LevelUtil {

    /**
     * 部门层级的分隔符
     */
    private final static String SEPARATOR = ".";

    /**
     * 根部门，部门的root们
     */
    public final static String ROOT = "0";

    /**
     * 得到部门标示符 0.1.1
     * 0 -> 首层
     * 0.1
     * 0.1.2
     * 0.1.3
     * 0.4
     * @param parentLevel: 父级部门
     * @param parentId：    部门id
     */
    public static String calculateLevel(String parentLevel, int parentId) {
        //父层级为空，返回0，表示root部门
        if (StringUtils.isBlank(parentLevel)) {
            return ROOT;
        } else {
             // join方法示例
             //1 private static final String[] str = {"1","2","3","4"};
             //2 String str2 = StringUtils.join(str, "|");
             //3
             //4 System.out.println(str2);
             //5 输出结果：1|2|3|4
            return StringUtils.join(parentLevel, SEPARATOR, parentId);
        }
    }
}
