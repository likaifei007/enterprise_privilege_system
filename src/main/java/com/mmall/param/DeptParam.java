package com.mmall.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * 部门参数
 */
@Getter
@Setter
@ToString
public class DeptParam {

    /**
     * Id
     */
    private Integer id;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不可以为空")
    @Length(max = 15, min = 2, message = "部门名称长度需要在2-15个字之间")
    private String name;

    /**
     * 上级部门的id
     */
    private Integer parentId = 0;

    /**
     * 展示顺序
     */
    @NotNull(message = "展示顺序不可以为空")
    private Integer seq;

    /**
     * 备注
     */
    @Length(max = 150, message = "备注的长度需要在150个字以内")
    private String remark;
}
