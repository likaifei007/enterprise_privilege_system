package com.mmall.dto;

import com.google.common.collect.Lists;
import com.mmall.model.SysDept;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * this class has one more property than SysDept for list of deptLevelDto
 */
@Getter
@Setter
@ToString
public class DeptLevelDto extends SysDept {

    /**
     * 直系列子部门列表
     * {
     * "id":4,
     * "name":"UI设计",
     * "parentId":1,
     * "level":"0.1",
     * "seq":3,
     * "remark":"",
     * "operator":"system",
     * "operateTime":1507766143000,
     * "operateIp":"127.0.0.1",
     * "deptList":[
     *      {
     *          "id":15,
     *          "name":"photoshopTeam",
     *          "parentId":4,
     *          "level":"0.1.4",
     *          "seq":1,
     *          "remark":"ps团队",
     *          "operator":"Admin",
     *          "operateTime":1543754439000,
     *          "operateIp":"0:0:0:0:0:0:0:1",
     *          "deptList":[
     *          <p>
     *          ]
     *      },
     *      {
     *          "id":16,
     *          "name":"手工绘图团队",
     *          "parentId":4,
     *          "level":"0.1.4",
     *          "seq":1,
     *          "remark":"绘图团队",
     *          "operator":"Admin",
     *          "operateTime":1543754513000,
     *          "operateIp":"0:0:0:0:0:0:0:1",
     *          "deptList":[
     *            <p>
     *           ]
     *       }
     *      ]
     * }
     */
    private List<DeptLevelDto> deptList = Lists.newArrayList();

    /**
     * 只属性拷贝的作用
     * 该方法将dept的属性值通过BeanUtils.copyProperties(dept, dto)拷贝到dto
     */
    public static DeptLevelDto adapt(SysDept dept) {
        DeptLevelDto dto = new DeptLevelDto();
        BeanUtils.copyProperties(dept, dto);
        return dto;
    }
}
