package com.mmall.model;

/**
 *  当表中有text类型的字段的时候mybatis generator会多生成一个类
 *  ***WithBLOBs text 类型相对消耗性能，且通常这种类型的字段一般用来作为备注用的
 *  使用频率不高如果每次都取出来对系统开销较大
 *  生成两个类可以让开发者去根据需要选择不同的两个类
 */
public class SysLogWithBLOBs extends SysLog {
    private String oldValue;

    private String newValue;

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue == null ? null : oldValue.trim();
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue == null ? null : newValue.trim();
    }
}